const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', ((req, res) => {
    const items = fileDb.getItems();
    res.send(items);
}));

router.post('/', ((req, res) => {
    if (!req.body.message) {
        return res.status(400).send({error: 'Data not valid'});
    }
    const newItem = fileDb.addItem({
        message:req.body.message
    })
    res.send(newItem);
}));

module.exports = router;