const express = require('express');
const messages = require('./app/messages');

const app = express();
app.use(express.json());

const port = 8080;

app.use('/messages', messages);

app.listen(port, ()=> {
    console.log('Server works on: ' + port);
})