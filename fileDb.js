const fs = require('fs');
const messages = './messages';

module.exports = {
    getItems() {
        const array = [];
        const files =  fs.readdirSync(messages);
        files.forEach(item => {
            array.push(JSON.parse(fs.readFileSync(`${messages}/${item}`)));
        })

        if (array.length > 5) {
            return array.slice(-5);
        } else {
            return array;
        }
    },
    addItem(item) {
        item.datetime = new Date().toISOString();
        this.save(`${messages}/${item.datetime}.txt`, item);
        return item;
    },
    save(filename, item) {
        fs.writeFileSync(filename, JSON.stringify(item));
    }
};